<?php
include_once "data.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h4>Contoh Tabel</h4>
    <table width='100%' border="1">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Jenis Kelamin</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
            foreach ($data as $row) {
            ?>
                <tr>
                    <td><?= $no++ ?></td>
                    <td><?= $row['Nama'] ?></td>
                    <td><?= $row['Alamat'] ?></td>
                    <td><?= $row['Jenis Kelamin'] ?></td>
                </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
</body>
</html>