<?php
$data = [
    [
        'Nama' => 'John Doe',
        'Alamat' => 'Jl. Jendral Sudirman No. 123',
        'Jenis Kelamin' => 'Laki-laki'
    ],
    [
        'Nama' => 'Jane Smith',
        'Alamat' => 'Jl. Pangeran Diponegoro No. 45',
        'Jenis Kelamin' => 'Perempuan'
    ],
    [
        'Nama' => 'David Johnson',
        'Alamat' => 'Jl. Gatot Subroto No. 67',
        'Jenis Kelamin' => 'Laki-laki'
    ],
    [
        'Nama' => 'Emily Davis',
        'Alamat' => 'Jl. Merdeka Raya No. 89',
        'Jenis Kelamin' => 'Perempuan'
    ],
    [
        'Nama' => 'Michael Wilson',
        'Alamat' => 'Jl. Thamrin No. 34',
        'Jenis Kelamin' => 'Laki-laki'
    ],
    [
        'Nama' => 'Sarah Brown',
        'Alamat' => 'Jl. Kebon Sirih No. 56',
        'Jenis Kelamin' => 'Perempuan'
    ],
    [
        'Nama' => 'Robert Lee',
        'Alamat' => 'Jl. Hayam Wuruk No. 78',
        'Jenis Kelamin' => 'Laki-laki'
    ],
    [
        'Nama' => 'Lisa Anderson',
        'Alamat' => 'Jl. Diponegoro No. 12',
        'Jenis Kelamin' => 'Perempuan'
    ],
    [
        'Nama' => 'William Martinez',
        'Alamat' => 'Jl. Panglima Polim No. 21',
        'Jenis Kelamin' => 'Laki-laki'
    ],
    [
        'Nama' => 'Jennifer Taylor',
        'Alamat' => 'Jl. Rasuna Said No. 33',
        'Jenis Kelamin' => 'Perempuan'
    ]
];
